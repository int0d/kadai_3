<?php

class IndexController extends Phalcon\Mvc\Controller {
    
    // ホーム画面
    public function indexAction() {
        
        if ( $this->session->get('login') ){
            // ログインしている

            // ユーザー情報など表示
            $this->view->なまえ = $this->session->なまえ;
            $this->view->token = $this->session->token;

            // トークンを発行

        }else{
            // ログインしていない場合
            $this->response->redirect('login/unauthorized');
        }

    }

}