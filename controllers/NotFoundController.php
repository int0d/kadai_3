<?php

class NotFoundController extends Phalcon\Mvc\Controller {
    // 404
    public function notFoundAction(){
        $this->response->setStatusCode(404);
    }
}
