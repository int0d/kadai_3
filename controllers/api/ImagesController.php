<?php

// 画像用。
// 画像を指定URLからダウンロードし、画像をファイルに保存
class ImagesController extends ApiController {

    const IMAGE_DIR = './images/';

    public function addAction() {

        // リクエストは生画像データ
        $data = (array)$this->request->getRawBody();

        // ファイル形式を判定: imege/*** のみ受理
        $mime = (new finfo(FILEINFO_MIME_TYPE))->buffer($data[0]);
        if(strncmp($mime, 'image/', 6) != 0){
            return $this->jsonResponse(null, 415); // Unsupported Media Type
        }

        // 中身のハッシュ値をファイル名として使用
        $filename = hash('sha256', $data[0]);
        file_put_contents(self::IMAGE_DIR.'/'.$filename, $data[0], LOCK_EX);

        // 保存先を返す
        $response = $this->jsonResponse(null, 201);
        $response->setHeader('Location', '/images/'.$filename);
        return $response;
    }

    public function getAction($filename){
        // JSONにしないで生で返す
        if(!file_exists(self::IMAGE_DIR.'/'.$filename)){
            return $this->jsonResponse(null, 404);
        }

        $data = file_get_contents(self::IMAGE_DIR.'/'.$filename);
        $mime = (new finfo(FILEINFO_MIME_TYPE))->buffer($data);

        $response = new \Phalcon\Http\Response();
        $response->setContent($data);
        $response->setContentType($mime);
        return $response;
    }

    public function deleteAction($filename){
        if(!file_exists(self::IMAGE_DIR.'/'.$filename)){
            return $this->jsonResponse(null, 404);
        }
        
        unlink(self::IMAGE_DIR.'/'.$filename);
    }
}