<?php

class ApiController extends Phalcon\Mvc\Controller {

    const JWS_SECRET = 'abcd'; // 署名用の鍵

    function beforeExecuteRoute(){
        if(!is_subclass_of($this, 'ApiController')){ // 循環防止
            return;
        }
        // 認証
        // 署名の検証を行う
        $jwt = $this->request->getHeader('Authorization');

        // トークンが渡されていない
        if(!$jwt){
            return $this->dispatcher->forward(
                ['controller' => 'api', 'action' => 'unauthorized', 'params'=>['認証トークンが必要です。']]
            );
        }

        // 署名の検証
        $jwt_e = explode('.', $jwt);
        
        if(count($jwt_e) != 3){
            return $this->dispatcher->forward(
                ['controller' => 'api', 'action' => 'unauthorized', 'params'=>['認証トークンが不正です。']]
            );
        }

        $base64url_encode = function($data){return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); };
        $jws_signature = hash_hmac('sha256', $jwt_e[0].'.'.$jwt_e[1], ApiController::JWS_SECRET, true);

        if($base64url_encode($jws_signature) != $jwt_e[2]){
            return $this->dispatcher->forward(
                ['controller' => 'api', 'action' => 'unauthorized', 'params'=>['署名の検証に失敗しました。']]
            );
        }
        
    }

    function unauthorizedAction($message = '不明なエラー。'){
        return $this->jsonResponse(['errors' => ['message' => $message]], 401);
    }
    
    function jsonResponse($data, $code=200){
        $response = new \Phalcon\Http\Response();
        $response->setStatusCode($code);
        #if(isset($data)){
        $response->setContentType('application/json', 'UTF-8');
        $response->setContent(json_encode($data));
        #}
        return $response;
    }
}