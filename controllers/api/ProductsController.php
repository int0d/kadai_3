<?php

class ProductsController extends ApiController {
    
    // 個別取得
    public function getAction($id=null) {
        if( $data = Products::findFirst(["id = {$id}"]) ){
            return $this->jsonResponse($data, 200);
        }else{
            return $this->jsonResponse(null, 404);
        }
    }

    // リスト取得
    public function indexAction($id=null) {
        return $this->jsonResponse(Products::find());
    }

    // 検索
    public function searchAction() {
        $q = $this->request->get('q');
        return $this->jsonResponse(Products::searchByNameAndDescription($q));
    }

    // 追加(id生成) POST
    public function addAction() {
        $data = (array)$this->request->getJsonRawBody();
        $p = new Products();

        // IDは指定してはならない
        if(isset($data['id'])){
            return $this->jsonResponse(['errors' => ['message' => 'id は設定できません。']], 400);
        }
        
        if( $p->save($data) ) {
            // Location返す
            $response = $this->jsonResponse(null, 201);
            $response->setHeader('Location', '/products/'.$p->id);
            return $response;
        } else {
            // save失敗
            $messages = $p->getMessages();
            $messages_text = array_map(
                function($m){return ['message' => $m->getMessage()];},
                $messages
            );
            return $this->jsonResponse(['errors' => $messages_text], 400);
        }
    }

    // 更新 PUT
    public function updateAction($id) {
        $data = (array)$this->request->getJsonRawBody();
        $p = Products::findFirst(["id = {$id}"]);
        
        // 該当する商品がない
        if(!$p){
            return $this->jsonResponse(null, 404);
        }

        if(isset($data['id'])){
            return $this->jsonResponse(['errors' => ['message' => 'id は変更できません。']], 400);
        }

        if( $p->update($data) ) {
            return $this->jsonResponse(null, 204);
        } else {
            // save失敗
            $messages = $p->getMessages();
            $messages_text = array_map(
                function($m){return ['message' => $m->getMessage()];},
                $messages
            );
            return $this->jsonResponse(['errors' => $messages_text], 400);
        }
    }

    // 削除
    public function deleteAction($id) {
        $p = Products::findFirst(["id = {$id}"]);

        if(!$p){
            return $this->jsonResponse(null, 404);
        }
        
        if( $p->delete() ){
            return $this->jsonResponse(null, 204);
        }else{
            return $this->jsonResponse(null, 500);
        }
    }
    
}