<?php

class NotFoundController extends ApiController {
    // 404
    public function notFoundAction(){
        return $this->jsonResponse(['errors' => ['message'=>'アクションが見つかりません']], 404);
    }
}