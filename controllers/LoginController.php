<?php

class LoginController extends Phalcon\Mvc\Controller {

    const CLIENT_ID = '747e279fcf8a5b40a3b2';
    const CLIENT_SECRET = 'be83e1f5c7c1980865a5a1d94829a120985a235e';
    
    // 1. ログイン画面
    public function indexAction() {
        $this->view->client_id = self::CLIENT_ID;
        $this->view->test = 'abc';
    }

    // 2. アクセストークンを取得 & ユーザ情報を取得
    public function githubCallbackAction(){
        // 2.1 アクセストークンを取得
        $code =  $this->request->get('code');
        // code がない場合
        $res = @file_get_contents(
            'https://github.com/login/oauth/access_token',
            false,
            stream_context_create(
                ['http'=>[
                    'method' => 'POST',
                    'header' => 'Content-Type: application/x-www-form-urlencoded',
                    'content' => http_build_query(
                        ['code'=>$code, 'client_id'=>self::CLIENT_ID, 'client_secret'=>self::CLIENT_SECRET]
                    )
                ]]
            )
        );

        if($res === false){
            // 失敗した場合
            $this->view->errorMessage = 'アクセストークンを取得できませんでした。';
            $this->view->render('login','error');
            $this->view->disable();
            return;
        }

        parse_str($res, $tmp);
        if( isset($tmp['access_token']) ){
            $token = $tmp['access_token'];
        }else{
            $this->view->errorMessage = 'アクセストークンを取得できませんでした。';
            $this->view->render('login','error');
            $this->view->disable();
            return;
        }

        // 2.2 ユーザ情報取得
        $res = file_get_contents(
            "https://api.github.com/user?access_token=$token",
            false,
            stream_context_create(
                ['http'=>[
                    'method' => 'GET',
                    'header' => 'User-Agent: kadai_2'
                ]]
            )
        );

        if($res === false){
            // 失敗した場合
            $this->view->errorMessage = 'ユーザ情報を取得できませんでした。';
            $this->view->render('login','error');
            $this->view->disable();
            return;
        }

        $user_info = json_decode($res);

        // 2.3 DBに入れる(略)
        // 今回は自作サービスのリソースとの結びつけはしていない。

        // 2.3' トークンを発行
        // ASCII以外を用いる場合は文字コードに注意
        $base64url_encode = function($data){
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
        };
        $jws_header = "{\"typ\":\"JWT\",\n\"alg\":\"HS256\"}";
        $jws_payload = "{\"sub\":\"$user_info->id@GitHub\"}";
        $jws_sining_input = $base64url_encode($jws_header).'.'.$base64url_encode($jws_payload);
        $jws_signature = hash_hmac('sha256', $jws_sining_input, ApiController::JWS_SECRET, true);

        $jwt = $jws_sining_input.'.'.$base64url_encode($jws_signature);
        
        // 2.4 セッションを設定して元画面へ戻る
        if(isset($user_info->name)){
            // 認証OK
            $this->session->login = true;
            $this->session->なまえ = $user_info->name; // メイン画面での表示用
            $this->session->token = $jwt;

            $this->response->redirect('');
        }else{
            $this->view->errorMessage = 'ユーザ情報を取得できませんでした。';
            $this->view->render('login','error');
            $this->view->disable();
            return;
        }

    }

    public function logoutAction(){
        $this->session->destroy();
        $this->response->redirect('login');
    }

    public function unauthorizedAction() {
        $this->response->setStatusCode(401);
    }

}