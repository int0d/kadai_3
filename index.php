<?php

$loader = new Phalcon\Loader();
$loader->registerDirs(['.','./models','./controllers','./controllers/api']);
$loader->register();

$di = new Phalcon\Di\FactoryDefault();

$di->set(
    'db',
    function(){
        return new Phalcon\Db\Adapter\Pdo\Mysql( ['host' => 'localhost', 'username' => 'root', 'dbname' => 'kadai']);
    }
);

$di->set(
    'url',
    function(){
        $url = new Phalcon\Mvc\Url();
        $url->setBaseUri('http://localhost:8083/');
        return $url;
    }
);

$di->set(
    'router',
    function(){
        $router = new Phalcon\Mvc\Router(false);
        $router->removeExtraSlashes(true);
        $router->addGet('/', ['controller' => 'index', 'action' => 'index']);
        $router->addGet('/login', ['controller' => 'login', 'action' => 'index']);
        $router->addGet('/login/unauthorized', ['controller' => 'login', 'action' => 'unauthorized']);
        $router->addGet('/login/githubCallback', ['controller' => 'login', 'action' => 'githubCallback']);
        $router->addGet('/logout', ['controller' => 'login', 'action' => 'logout']);
        $router->notFound(['controller' => 'notFound', 'action' => 'notFound']);

        // API
        $router->addGet('/api/products', ['controller' => 'products', 'action' => 'index']);
        $router->addPost('/api/products', ['controller' => 'products', 'action' => 'add']);
        $router->addGet('/api/products/search', ['controller' => 'products', 'action' => 'search']);
        $router->addGet('/api/products/{id:\d+}', ['controller' => 'products', 'action' => 'get']);
        $router->addPut('/api/products/{id:\d+}', ['controller' => 'products', 'action' => 'update']);
        $router->addDelete('/api/products/{id:\d+}', ['controller' => 'products', 'action' => 'delete']);

        $router->addPost('/api/images', ['controller' => 'images', 'action' => 'add']);
        $router->addGet('/api/images/{filename:[0-9a-fA-F]{64}}', ['controller' => 'images', 'action' => 'get']);
        $router->addDelete('/api/images/{filename:[0-9a-fA-F]{64}}', ['controller' => 'images', 'action' => 'delete']);

        return $router;
    }
);

$di->set(
    'view',
    function(){
        $view = new Phalcon\Mvc\View();
        $view->setViewsDir('./views');
        return $view;
    }
);

$di->set(
    'session',
    function(){
        $session = new Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    }
);

$router = $di['router'];
$router->handle();

$application = new Phalcon\Mvc\Application($di);

try{
    $response = $application->handle();
    $response->send();
} catch (Exception $e) {
    $response = new Phalcon\Http\Response();
    $response->setStatusCode(500);
    $response->setContent($e);
    $response->send();
    die();
}
