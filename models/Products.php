<?php

/* テーブル構造

CREATE TABLE `kadai`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(500) NULL,
  `price` DECIMAL(20,8) NULL,
  `image` VARCHAR(1000) NULL,
  PRIMARY KEY (`id`));

*/

class Products extends Phalcon\Mvc\Model {

    // 部分一致検索
    function searchByNameAndDescription($q){
      return Products::find(["name LIKE '%{$q}%' or description LIKE '%{$q}%'"]);
    }

    public function validation(){
      $validator = new Phalcon\Validation();

      $validator->add('name', new Phalcon\Validation\Validator\StringLength([
        'max' => 100,
        'messageMaximum' => ':field は100文字以内で指定してください。',
        'allowEmpty' => true
      ]));

      $validator->add('description', new Phalcon\Validation\Validator\StringLength([
        'max' => 500,
        'messageMaximum' => ':field は500文字以内で指定してください。',
        'allowEmpty' => true
      ]));

      $validator->add('price', new Phalcon\Validation\Validator\RegEx([
        'pattern' => '/^([0-9]{1,12}\.[0-9]{1,8}|[0-9]{1,12}\.?|\.[0-9]{1,8})$/',
        // 桁数が多い場合精度が落ちるphalconのバグがある: https://github.com/phalcon/cphalcon/issues/13151
        'message' => ':field は整数部分12桁，小数部分8桁以内で指定してください。',
        'allowEmpty' => true
      ]));

      $validator->add('image', new Phalcon\Validation\Validator\StringLength([
        'max' => 1000,
        'messageMaximum' => ':field は1000文字以内で指定してください。',
        'allowEmpty' => true
      ]));

      $validator->add('image', new Phalcon\Validation\Validator\Url([
        'message' => ':field には有効なURLを指定してください。',
        'allowEmpty' => true
      ]));

      return $this->validate($validator);
    }

}